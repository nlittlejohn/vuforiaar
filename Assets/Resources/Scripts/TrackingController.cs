﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Vuforia;

public class TrackingController : MonoBehaviour, ITrackableEventHandler {
    private TrackableBehaviour trackableBehaviour;
    private string _accessToken;
    private string _hostUrl = "https://sd.kcftech.com/api2";
    private string _apiUserName = "chris77carl";
    private string _apiPassword = "lr@csirhC.77";

    // Start is called before the first frame update
    void Start()
    {
        trackableBehaviour = GetComponent<TrackableBehaviour>();
        if (trackableBehaviour) {
            trackableBehaviour.RegisterTrackableEventHandler(this);
        }

        GenerateAccessTokenAsync();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus) {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            OnTrackingFound();
        else
            onTrackingLost();
    }

    private void OnTrackingFound() {
        //Debug.Log("@@@@ ON TRACKING FOUND @@@@: ");
        StartCoroutine(GetText());
    }

    private void onTrackingLost() {}


    private async Task GenerateAccessTokenAsync() {
        var url = _hostUrl + "/token/";

        var request = (HttpWebRequest)WebRequest.Create(url);
        request.ContentType = "application/json";
        request.Method = "POST";

        using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
            string json = "{\"Username\":\"" + _apiUserName + "\",\"Password\":\"" + _apiPassword + "\"}";
            streamWriter.Write(json);
            streamWriter.Flush();
            streamWriter.Close();
        }

        var response = await request.GetResponseAsync();

        using (var responseStream = response.GetResponseStream()) {
            if (responseStream == null) {
                Debug.Log("RESPONSE WAS NULL");
                return;
            }
            using (var reader = new StreamReader(responseStream, Encoding.UTF8)) {
                JObject result = JObject.Parse(reader.ReadToEnd());
                _accessToken = (string)result["token"];
                //Debug.Log("_accessToken: " + _accessToken);
            }
        }
    }

    private Stream GetResponseStream(string url, bool isPost) {
        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Headers.Add("Authorization", "Bearer " + _accessToken);

        if (isPost) {
            request.ContentType = "application/json";
            request.Method = "POST";
        }

        try {
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream()) {
                if (responseStream == null) {
                    return null;
                }
                return responseStream;
            }
        }
        catch (WebException ex) {
            return null;
        }
    }

    IEnumerator GetText() {
        //UnityWebRequest request = UnityWebRequest.Get("https://sd.kcftech.com/api2/collectors/2142");
        UnityWebRequest request = UnityWebRequest.Get("https://sd.kcftech.com/api2/collectors/2142/slim");

        if (_accessToken != null) {
            request.SetRequestHeader("Authorization", "Bearer " + _accessToken);
        }
        else {
            Debug.Log("ERROR! Acess Token is null!");
        }

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError) {
            Debug.Log(request.error);
        }
        else {
            // Show results as text
            //Debug.Log(request.downloadHandler.text);
            //GetComponent<TextMesh>().text = request.downloadHandler.text;
            Node node = JsonConvert.DeserializeObject<Node>(request.downloadHandler.text);
            //Debug.Log(node.Id + " | " + node.SerialNumber);
            RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(request.downloadHandler.text);
            //Debug.Log(rootObject.SerialNumber);
            
            GameObject.FindWithTag("serialnumber").GetComponent<TextMesh>().text = "Serial Number: " + node.SerialNumber;
            GameObject.FindWithTag("isonline").GetComponent<TextMesh>().text = "Status: " + (rootObject.IsOnline ? "Online" : "Offline");

            int receiverNodeCount = 0;
            foreach (Receiver receiver in rootObject.Receivers) {
                receiverNodeCount += receiver.Nodes.Count;
            }
            GameObject.FindWithTag("connectednodes").GetComponent<TextMesh>().text = "Nodes: " + rootObject.Receivers.Count.ToString();
            GameObject.FindWithTag("basestationip").GetComponent<TextMesh>().text = "IP: " + (rootObject.IPAddress != "" ? rootObject.IPAddress : "192.168.1.180");
            GameObject.FindWithTag("nickname").GetComponent<TextMesh>().text = "Nickname: " + rootObject.Nickname;
            //this.GetComponentInChildren<TextMesh>().text = request.downloadHandler.text;
        }
    }
}

public class Node {
    public int Id { get; set; }
    public string SerialNumber { get; set; }
}

public class Receiver {
    public List<Node> Nodes { get; set; }
    public bool IsBaseStationReceiver { get; set; }
    public int Id { get; set; }
    public string SerialNumber { get; set; }
}

public class SlimCollector {
    public int Id { get; set; }
    public bool IsOnline { get; set; }
    public string FriendlyName { get; set; }
    public string SerialNumber { get; set; }
}

public class AvailableReceiver {
    public bool IsBaseStationReceiver { get; set; }
    public int Id { get; set; }
    public string SerialNumber { get; set; }
}

public class EthernetReceivers {
    public List<AvailableReceiver> AvailableReceivers { get; set; }
    public List<object> LockedReceivers { get; set; }
    public List<object> StrayReceivers { get; set; }
}

public class RootObject {
    public string SerialNumber { get; set; }
    public string Nickname { get; set; }
    public string CurrentSoftwareVersion { get; set; }
    public int RfAggressiveness { get; set; }
    public string AllowedReceiverFrequencies { get; set; }
    public DateTime LastUpdate { get; set; }
    public DateTime LastDataUpdate { get; set; }
    public bool IsOnline { get; set; }
    public List<Receiver> Receivers { get; set; }
    public List<SlimCollector> SlimCollectors { get; set; }
    public EthernetReceivers EthernetReceivers { get; set; }
    public string IPAddress { get; set; }
    public bool RepeaterLockingEnabled { get; set; }
}
