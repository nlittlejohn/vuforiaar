﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Vuforia;

public class PumpInfoController : MonoBehaviour, ITrackableEventHandler {
    private TrackableBehaviour mTrackableBehaviour;

    private string _accessToken;
    private string _hostUrl = "https://sd.kcftech.com/api2";
    private string _apiUserName = "chris77carl";
    private string _apiPassword = "lr@csirhC.77";

    // Start is called before the first frame update
    void Start() {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        
        GenerateAccessTokenAsync();
        //StartCoroutine(GetText());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus) {
        Debug.Log("OnTrackableStateChanged");
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED) {
            Debug.Log("OnTrackableStateChanged state: " + newStatus);
        }
    }

    private void OnTrackingFound() {
        Debug.Log("ON TRACKING FOUND");
    }

    private async Task GenerateAccessTokenAsync() {
        var url = _hostUrl + "/token/";

        //var webRequest = new UnityWebRequest();
        //webRequest.url = url;
        //webRequest.method = UnityWebRequest.kHttpVerbPOST;

        var request = (HttpWebRequest)WebRequest.Create(url);
        request.ContentType = "application/json";
        request.Method = "POST";

        using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
            string json = "{\"Username\":\"" + _apiUserName + "\",\"Password\":\"" + _apiPassword + "\"}";
            streamWriter.Write(json);
            streamWriter.Flush();
            streamWriter.Close();
        }

        var response = await request.GetResponseAsync();

        using (var responseStream = response.GetResponseStream()) {
            if (responseStream == null) {
                Debug.Log("RESPONSE WAS NULL");
                return;
            }
            using (var reader = new StreamReader(responseStream, Encoding.UTF8)) {
                JObject result = JObject.Parse(reader.ReadToEnd());
                _accessToken = (string)result["token"];
                //Debug.Log("_accessToken: " + _accessToken);
            }
        }
    }

    private Stream GetResponseStream(string url, bool isPost) {
        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Headers.Add("Authorization", "Bearer " + _accessToken);

        if (isPost) {
            request.ContentType = "application/json";
            request.Method = "POST";
        }

        try {
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream()) {
                if (responseStream == null) {
                    return null;
                }
                return responseStream;
            }
        }
        catch (WebException ex) {
            return null;
        }
    }

    IEnumerator GetText() {
        Debug.Log("GET TEXT!");
        //UnityWebRequest request = UnityWebRequest.Get("http://www.google.com");
        UnityWebRequest request = UnityWebRequest.Get("https://sd.kcftech.com/api2/collectors/2142/slim");

        if (_accessToken != null) {
            request.SetRequestHeader("Authorization", "Bearer " + _accessToken);
        }
        else {
            Debug.Log("ERROR! Acess Token is null!");
        }

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError) {
            Debug.Log(request.error);
        }
        else {
            // Show results as text
            Debug.Log(request.downloadHandler.text);
            GetComponent<TextMesh>().text = request.downloadHandler.text;
        }
    }
}
